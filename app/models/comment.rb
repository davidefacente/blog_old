class Comment < ActiveRecord::Base
  attr_accessible :body, :post_it
  belongs_to :post
  validates_presence_of :post_id
  validates_presence_of :body
end
